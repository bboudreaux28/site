---
title: G.A.P. Seminar
---

**Time: Thursdays from 10:30am to 11:30am EDT (New Time!)**

**Location: Middlesex College 108**

**Zoom Link:** Starting the second talk, there will be a proper Zoom broadcast for the G.A.P. Seminar for those who are self-isolating or who are otherwise unable to attend physically. However in-person attendance is preferred, as we are still working out the technical details regarding Zoom broadcasts. The Zoom link is https://westernuniversity.zoom.us/j/95828777167 (no password).

The Geometry, Analysis, and Physics Seminar (or G.A.P. Seminar) is being organized by [Michael Francis](https://publish.uwo.ca/~mfranc65/) and myself over the course of the Fall 2021 Semester.
If you are interested in giving a talk at the seminar please let us know!

# Talks

## Talk 9: Factorization of Dirac operators along a submersion

- Recording: [Click here](https://uwoca-my.sharepoint.com/:v:/g/personal/mfranc65_uwo_ca/EdMPi8gXO_xKsWzZp6d1HY0Bh5y9ueevOLrrfS4dFK0e4g?e=363Vns)
- Date: December 2, 2021
- Speaker: [Luuk Verhoeven](https://www.math.uwo.ca/people/Graduate%20Students.html)
- Institution: [University of Western Ontario](https://www.math.uwo.ca)
- Abstract:

>Spectral triples `$(A, H, D)$`. can be interpreted as unbounded representatives for classes
>in `$KK$` theory, specifically in `$KK(A,\mathbb{C})$`. It therefore seems natural to investigate if, and how,
>constructions from `$KK$`-theory are reflected back in noncommutative geometry. In this talk we will
>look at a specific case of this; given a submersion `$\pi:M\to B$` there is a class, `$\pi_!$`, in `$KK(C(M),C(B))$`
>such that there is a Kasparov product `$[M] = \pi_! x [B]$`. In this talk we will cover an
>article by W. van Suijlekom and J. Kaad on how this Kasparov product works at the level of
>spectral triples and correspondences. It turns out that the factorization is exact, up to a
>curvature term.


## Talk 8: Dirac Ensembles

- Recording: [Click here](https://uwoca-my.sharepoint.com/:v:/g/personal/mfranc65_uwo_ca/ERdSwmlESJJIvMUH_Rh91k4BM_ihlGf0h-Ly-RCfKzv_Zw?e=gXueyu)
- Date: November 25, 2021
- Speaker: [Nathan Pagliaroli](https://www.math.uwo.ca/people/Graduate%20Students.html)
- Institution: [University of Western Ontario](https://www.math.uwo.ca)
- Abstract:

> In this talk I will give a brief introduction to random matrix theory and Dirac ensembles. We will discuss their spectral statistics, relationship to random matrix ensembles, and the use of a procedure known as "Topological Recursion".

## Talk 7: Bootstrapping Dirac Ensembles

- Recording: [Click here](https://uwoca-my.sharepoint.com/:v:/g/personal/mfranc65_uwo_ca/ETb4BaCsSWdLh8FRCGgW6ysBthy49YhzaJxNXLMTWdrHFg?e=dKJMMI)
- Date: November 18, 2021
- Speaker: [Hamed Hessam](https://www.math.uwo.ca/people/Graduate%20Students.html)
- Institution: [University of Western Ontario](https://www.math.uwo.ca)
- Abstract:

> In this talk we will see a technique called bootstraps can be applied to find the moments of a single matrix and two-matrix models taken from finite noncommutative geometries. We will discuss the relationships between the order parameter of the model and the second moment. From there all other moments are able to be expressed as in terms of the order parameter and the second moment, allowing them to be computed. This work is based on the joint paper of mine with Masoud Khalkhali and Nathan Pagliaroli.

## Talk 6: Survey of local theory of singular Levi-flat hypersurfaces

- Recording: [Click Here](https://uwoca-my.sharepoint.com/:v:/g/personal/mfranc65_uwo_ca/Ecb7EUPXpwtDnZgPifMGDG4Bsc1Gc_HrGBAkHN4gTeoVHw?e=oAcfla)
- Date: November 11, 2021
- Speaker: [Rasul Shafikov](https://math.sci.uwo.ca/~shafikov/)
- Institution: [University of Western Ontario](https://www.math.uwo.ca)
- Abstract:

>Levi-flat hypersurfaces are important in the context of minimal sets of holomorphic foliations in complex projective spaces. It is well-known that closed smooth real analytic Levi-flats do not exist in `$\mathbb C \mathbb P^n$` for `$n>2$`, and it is an open problem if such hypersurfaces exist in `$\mathbb C \mathbb P^2$`. On the other hand, singular real-analytic Levi-flats exist in any dimension, and we discuss some recent developments in this subject.

## Talk 5: Groupoids and Algebras of Foliations: Part II

- Date: October 28, 2021
- Speaker: [Michael Francis](https://www.math.uwo.ca/people/postdocs.html)
- Institution: [University of Western Ontario](https://www.math.uwo.ca)
- Abstract:

>Last time, we defined (possibly singular) foliations to be certain
>collections of vector fields. It was emphasized that, defined this way, singular
>foliations are not uniquely determined their leaves. In this sequel talk, I will
>discuss a class of singular foliations I considered in my PhD thesis. These foliations
>have only two or three leaves total: a closed hypersurface (the singular leaf) and the
>components of its complement. Depending which vector fields gave the partition,
>however, interesting holonomy can result along the singular leaf. It turns out this
>holonomy can be used to completely classify such foliations (localized around the
>singular leaf). If time permits, I will talk about a question I am currently thinking
>about: under a suitable orientation hypothesis, is the "fundamental class" of these
>foliations always nontrivial? The answer to this question hinges on a rather concrete
>index calculation.

## Talk 4: D-Modules for Analysts

- Recording: [Click Here](https://uwoca-my.sharepoint.com/:v:/g/personal/mfranc65_uwo_ca/ERUK9BieQXVMi3-_soeLZugB5dSH2dIMalJ555x_dxqnuw?e=3fPsby)
- Date: October 21, 2021
- Speaker: [Avi Steiner](https://sites.google.com/view/avi-steiner)
- Institution: [University of Western Ontario](https://www.math.uwo.ca)
- Abstract:

> The main idea of D-modules (that is, of algebraic analysis) is to re-interpret
systems of linear PDEs as modules over a ring of differential operators. You then
prove things about these modules using the tools of algebra and algebraic
geometry, and then translate the results back into PDE language. An archetypal
example of such a result is the Cauchy—Kovalevskaya—Kashiwara theorem, which is a
vast generalization of the Cauchy—Kovalevskaya theorem from the classical study
of PDEs. I will give a sketch of the ideas that are needed to digest this
theorem. Along the way, you’ll be introduced to D-modules (of course!) and their
characteristic varieties.

## Talk 3: Weighted Bergman Kernels on Domains in `$\mathbb{C}^n$`: Part II

- Date: October 14, 2021
- Speaker: [Blake J. Boudreaux](https://bboudreaux28.gitlab.io/site)
- Institution: [University of Western Ontario](https://www.math.uwo.ca)
- Abstract:

> Given a domain `$\Omega\subseteq\mathbb{C}^n$`, the space of square-integrable holomorphic functions on `$\Omega$` is a Hilbert space with the standard inner product. This space is denoted by `$L^2_h(\Omega)$` and is known as the **Bergman space** of `$\Omega$`. It can be shown that the evaluation functionals `$E_z:L^2_h(\Omega)\to\mathbb{C}$` given by `$E_z(f)=f(z)$` are continuous on `$L^2_h(\Omega)$`, and hence via the Riesz representation theorem there exists `$K(\,\cdot\,,z)\in L^2_h(\Omega)$` that reproduces square-integrable holomorphic functions on `$\Omega$`. This function (on `$\Omega\times\Omega$`) is known as the **Bergman kernel** of `$\Omega$`, and has had a profound impact on the theory of holomorphic functions of several complex variables. This theory can also be generalized to weighted `$L^2$`-spaces, given that the weight function is sufficiently "nice".
>
>This will be a mostly expository talk on Bergman kernel, with an emphasis on weighted Bergman kernels. Time allowing I will sketch some work I have done regarding the zeroes of weighted Bergman kernels.

## Talk 2: Weighted Bergman Kernels on Domains in `$\mathbb{C}^n$`

- Recording: [Click Here](https://uwoca-my.sharepoint.com/personal/mfranc65_uwo_ca/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fmfranc65%5Fuwo%5Fca%2FDocuments%2FGAP%20Recordings%2F2021%2D10%2D07%20Boudreaux%2Emp4&parent=%2Fpersonal%2Fmfranc65%5Fuwo%5Fca%2FDocuments%2FGAP%20Recordings&originalPath=aHR0cHM6Ly91d29jYS1teS5zaGFyZXBvaW50LmNvbS86djovZy9wZXJzb25hbC9tZnJhbmM2NV91d29fY2EvRVZJNEdpUGlxdFpPbDhIUjh1SkFKbW9CVldYSHJfRlBXcmtRTmZMMFB3Q2Z0dz9ydGltZT0wYWcwN3NTSjJVZw)
- Date: October 7, 2021
- Speaker: [Blake J. Boudreaux](https://bboudreaux28.gitlab.io/site)
- Institution: [University of Western Ontario](https://www.math.uwo.ca)
- Abstract:

> Given a domain `$\Omega\subseteq\mathbb{C}^n$`, the space of square-integrable holomorphic functions on `$\Omega$` is a Hilbert space with the standard inner product. This space is denoted by `$L^2_h(\Omega)$` and is known as the **Bergman space** of `$\Omega$`. It can be shown that the evaluation functionals `$E_z:L^2_h(\Omega)\to\mathbb{C}$` given by `$E_z(f)=f(z)$` are continuous on `$L^2_h(\Omega)$`, and hence via the Riesz representation theorem there exists `$K(\,\cdot\,,z)\in L^2_h(\Omega)$` that reproduces square-integrable holomorphic functions on `$\Omega$`. This function (on `$\Omega\times\Omega$`) is known as the **Bergman kernel** of `$\Omega$`, and has had a profound impact on the theory of holomorphic functions of several complex variables. This theory can also be generalized to weighted `$L^2$`-spaces, given that the weight function is sufficiently "nice".
>
>This will be a mostly expository talk on Bergman kernel, with an emphasis on weighted Bergman kernels. Time allowing I will sketch some work I have done regarding the zeroes of weighted Bergman kernels.

## Talk 1: Groupoids and Algebras of Foliations

- Recording: [Click Here](https://uwoca-my.sharepoint.com/:v:/g/personal/mfranc65_uwo_ca/EQ_z63iEOF9Hqb7m4CjoCRsBswCGJplADjLK3ArDtrLogA?e=ActLTn)
- Date: September 20, 2021
- Speaker: [Michael Francis](https://www.math.uwo.ca/people/postdocs.html)
- Institution: [University of Western Ontario](https://www.math.uwo.ca)
- Abstract:

> Given a foliated manifold `$M$`, one feels that a leafwise smoothing operator on `$M$` ought to be represented by a smooth function on the subset of `$M\times M$` consisting of pairs of points lying on the same leaf. However, this subset may fail to be a manifold due to an interesting and important phenomenon known as **holonomy**. The **holonomy groupoid** of a foliation is, roughly speaking, what results when one attempts to smooth out this singular set, somewhat in the spirit of blowups in algebraic geometry. In this talk, I will attempt to explain the concept of holonomy and sketch some work I have done on the groupoids and algebras associated to singular foliations, based on work of Androulidakis and Skandalis.
