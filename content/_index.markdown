---
title: Home
---

<img src="/site/portrait2.png" style="max-width:15%;min-width:40px;float:right;" alt="portrait2.png" />
<img src="https://includes.uark.edu/assets/ua-logo-horz.svg" style="max-width:20%;min-width:50px;float:left;" alt="Western Science" />
<br>
<br>

# Blake J. Boudreaux
## Department of Mathematical Sciences

**Position:**	Visiting Assistant Professor\
**Office:**	SCEN 325\
**E-mail:**	[bb225@uark.edu](bb225@uark.edu)

I received my PhD from [Texas A&M University](https://www.tamu.edu/) in 2021 working under the supervision of [Harold Boas](https://haroldpboas.gitlab.io/). Afterwards, I spent three years as a postdoctoral fellow in the [Mathematics Department](https://math.uwo.ca) of [the University of Western Ontario](https://www.uwo.ca/) working with [Rasul Shafikov](https://math.sci.uwo.ca/~shafikov/). I am now (Fall 2024) beginning a position as a Visiting Assistant Professor of Mathematics at [the University of Arkansas](https://en.wikipedia.org/wiki/University_of_Arkansas), where I will be working with [Phil Harrington](https://math.uark.edu/directory/index/uid/psharrin/name/Phil+Harrington/) and [Andrew Raich](https://araich.hosted.uark.edu/).

My research area is the function theory of [several complex variables](https://en.wikipedia.org/wiki/Function_of_several_complex_variables). My original interest in the subject was in the function theory of square-integrable holomorphic functions---otherwise known as [Bergman spaces](https://en.wikipedia.org/wiki/Bergman_space)---as well as boundary regularity of holomorphic mappings. This naturally led to an interest in the [Bergman kernel](https://en.wikipedia.org/wiki/Bergman_kernel) function, the valuable [`$\bar\partial$`-Neumann operator](https://en.wikipedia.org/wiki/DBAR_problem), and well as the so-called Wiegerinck problem: *Is the dimension (as a vector space) of the Bergman space of a pseudoconvex domain infinite whenever it is nontrivial?* It was on these topics which I wrote my dissertation. Since starting as a postdoctoral fellow at Western University, I have developed an interest in polynomial and rational convexity and their generalizations to [Stein](https://en.wikipedia.org/wiki/Stein_manifold) and [complex projective manifolds](https://en.wikipedia.org/wiki/Projective_variety#Complex_projective_varieties).

This website was generated from [Markdown](https://en.wikipedia.org/wiki/Markdown) using [Hugo](https://gohugo.io/), an open-source static site generator, and is hosted on my [GitLab](https://gitlab.com/bboudreaux28/site.git). This page was last updated on December 08, 2024.
