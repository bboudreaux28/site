---
title: Teaching
---
# Fall 2024
### [College Algebra](https://catalog.uark.edu/undergraduatecatalog/coursesofinstruction/math/)
### [Elementary Differential Equations](https://catalog.uark.edu/undergraduatecatalog/coursesofinstruction/math/)

# Spring 2024
No Classes :-)

# Fall 2023
### [Calculus 1000](http://calculus.math.uwo.ca/)
### [Complex Analysis I (Undergrad/Grad Cross-listed)](https://www.math.uwo.ca/undergraduate/current_students/course_information/course-outlines-directory/course_outlines.html)

# Winter 2023
### [Calculus and Probability with Biological Applications](https://www.math.uwo.ca/undergraduate/current_students/course_information/course-outlines-directory/course_outlines.html)

# Fall 2022
### [Calculus 1000](http://calculus.math.uwo.ca/)

# Winter 2022
### [Calculus and Probability with Biological Applications](https://www.math.uwo.ca/undergraduate/current_students/course_information/course-outlines-directory/course_outlines.html)

# Fall 2021

### [Calculus 1000](http://calculus.math.uwo.ca/)

# Spring 2020 and Before

Classes taught in graduate school not indexed here (check [CV](https://bboudreaux28.gitlab.io/site/cv/)).
