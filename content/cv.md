---
title: Blake J. Boudreaux
---
<br>

| University of Arkansas              | bb225@uark.edu                       |
| :-----------------------------      | :----------------------------------- |
| Department of Mathematical Sciences | https://bboudreaux28.gitlab.io/site  |
| 850 West Dickson Street             | Phone: +1 (479) 575-3351             |
| SCEN Room 309                       |                                      |
| Fayetteville, AR 72701              |                                      |

# Education

- Texas A&M University
	- Ph.D., Mathematics, 2021
	- Advisor: Harold P. Boas
	- M.S., Mathematics, 2016
- Nicholls State University
	- B.S., Mathematics, 2013
# Employment

- University of Arkansas
	- Visiting Assistant Professor, 2024--present
- University of Western Ontario
	- Postdoctoral Fellow, 2021--2024
- Texas A&M University
	- Teaching Assistant, 2015--2021
- Nicholls State University
	- Graduate Assistant, 2013--2014
# Preprints/Publications

- ["`$T$`-polynomial convexity and holomorphic convexity"](https://arxiv.org/abs/2402.18692)
	- Ann. Fac. Toulouse Math. (to appear)
- ["Meromorphic convexity on Stein manifolds"](https://arxiv.org/abs/2310.07066) (joint with Rasul Shafikov)
	- Indiana Univ. Math. J. (to appear)
- ["Hypersurface convexity and extension of Kähler forms"](https://doi.org/10.48550/arXiv.2310.02132) (joint with Purvi Gupta and Rasul Shafikov)
	- Math. Z. (to appear)
- ["Relationships between the Bergman kernels of Hartogs domains and their base"](https://doi.org/10.48550/arXiv.2208.14291)
	- Preprint (Updated 2024). Submitted.
- ["On rational convexity of totally real sets"](https://doi.org/10.1112/blms.12842) (joint with Rasul Shafikov)
	- Bull. London Math. Soc. 55:5 (2023) 2158--2175.
- [Bergman Kernels, Hartogs Domains, and the Wiegerinck Problem](http://gateway.proquest.com/openurl?url_ver=Z39.88-2004&rft_val_fmt=info:ofi/fmt:kev:mtx:dissertation&res_dat=xri:pqm&rft_dat=xri:pqdiss:29241742)
	- Thesis (Ph.D.)--Texas A&M University. 2021.
- ["On the dimension of the Bergman space of some Hartogs domains with higher dimensional bases"](https://doi.org/10.1007/s12220-020-00557-1)
	- J Geom Anal. 31:8 (2021) 7885--7899.
- ["Equivalent Bergman spaces with inequivalent weights."](https://doi.org/10.1007/s12220-018-9986-5)
	- J Geom Anal 29:1 (2019) 217--223.

# Teaching

- University of Western Ontario
	- Instructor, Math 3124/9024 (Complex Analysis I---undergrad/grad cross-listed course), Fall 2023
	- Instructor, Calculus 1000, Fall 2023
	- Instructor, Applied Mathematics 1201, Winter 2023
	- Instructor, Calculus 1000, Fall 2022
	- Instructor, Applied Mathematics 1201, Winter 2022
	- Instructor, Calculus 1000, Fall 2021
- Texas A&M University
	- Instructor, Finite Mathematics, Summer 2021
	- Instructor, Preparatory Course for Complex Analysis Qualifying Examination, Summer 2020
	- Instructor, Finite Mathematics, Fall 2019
	- Instructor, Preparatory Course for Geometry/Topology Qualifying Examination, Summer 2018
	- Instructor, Topics in Contemporary Mathematics II, Spring 2018
	- Instructor, Preparatory Course for Geometry/Topology Qualifying Examination, Summer 2017
- Texas A&M University
	- Teaching Assistant, Engineering Mathematics II, Spring 2021
	- Teaching Assistant, Engineering Mathematics II, Spring 2020
	- Teaching Assistant, Finite Mathematics, Summer II 2019
	- Grader, Finite Mathematics, Summer I 2019
	- Grader, Differential Geometry I, Spring 2019
	- Teaching Assistant, Functions, Trignometry and Linear Systems, Fall 2018
	- Grader, Foundations of Mathematics, Fall 2017
	- Teaching Assistant, Engineering Mathematics I, Spring 2017
	- Grader, Theory of Functions of a Complex Variables II, Spring 2017
	- Teaching Assistant, Functions, Trignometry and Linear Systems, Fall 2016
	- Teaching Assistant, Engineering Mathematics I, Summer 2016
	- Grader, Theory of Functions of a Complex Variable II, Spring 2016
	- Grader, Theory of Functions of a Complex Variable I, Fall 2015
	- Teaching Assistant, Calculus, Spring 2015
	- Teaching Assistant, Analytic Geometry and Calculus, Fall 2014
- Nicholls State University
	- Mew Lab Tutor, Math 101, Fall 2013-Summer 2014

# Talks
- Banff International Research Station
	- *Convexity in Several Complex Variables*, Interactions between Symplectic and Holomorphic Convexity in 4 Dimensions, Spring 2023
- Central Michigan University
	- *On Rational Convexity of Totally Real Sets*, T.A.G. Seminar, Fall 2022
- Joint Mathematics Meetings
	- *Equivalent Bergman Spaces with Inequivalent Weights*, AMS Special Session on Several Complex Variables Geometric PDE and CR Geometry. Spring 2022
- Ohio State University, The
	- *Rational Convexity of Totally Real Sets*, Midwest SCV Conference, Spring 2023
- Syracuse University
	- *On Rational Convexity of Totally Real Sets*, Analysis Seminar, Fall 2022
- Texas A&M University
	- *Bergman Kernels, Hartogs Domains, and The Wiegerinck Problem*, Dissertation defense, Spring 2021
	- *The Boundary Behavior of Biholomorphic Maps: An Example by B. L. Fridman*, Several Complex Variables Seminar, Fall 2017
	- *The Szegő Kernel, Projection, and Applications*, Master's Presentation, Spring 2016
- University of Western Ontario
	- *Convexity in Several Complex Variables*, Analysis Seminar, Spring 2023
	- *Nef Line Bundles From a Curvature Perspective*, Analysis Seminar, Spring 2022
	- *Weighted Bergman Kernels on Domains in* `$\mathbb{C}^n$`, G.A.P. Seminar, Fall 2021
- Virtual East-West Several Complex Variables Seminar
	- *The Wiegerinck Problem in the Class of Hartogs Domains*, Spring 2021

# Service
- University of Western Ontario
	- Co-coordinator, Analysis Seminar, Academic Year 2023
	- Co-coordinator of Colloquium, Academic Year 2022
	- Co-coordinator (along with [Michael Francis](https://publish.uwo.ca/~mfranc65/), Geometry, Analysis, and Physics (GAP) Seminar, Fall 2021
- Texas A&M University
	- Host for Prospective Graduate Student, Spring 2019
	- Invited Panelist, First Year Graduate Student Seminar, Summer 2017
	- Coordinator, Several Complex Variables Seminar, Fall 2017-Summer 2021
- Referee Service
	- *Duke Mathematical Journal*
	- *Calculus of Variations and Partial Differential Equations*
	- *Complex Analysis and Operator Theory*
- Reviewer for MathSciNet
