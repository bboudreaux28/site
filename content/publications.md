---
title: Publications and Preprints
---

*Listed in order of appearance on [ArXiv.org](https://arxiv.org)*

## ["`$T$`-polynomial convexity and holomorphic convexity"](https://arxiv.org/abs/2402.18692)
- To appear in: *Annales de la Faculté des Sciences de Toulouse* (2025)
- Abstract:

> We compare the `$T$`-polynomial convexity of Guedj with holomorphic convexity away from the support of `$T$`. In particular we prove an Oka–Weil theorem for `$T$`-polynomial convexity. We also show a sufficient condition for when the notions of `$T$`-polynomial convexity and holomorphic convexity of `$X\setminus\text{Supp }T$` coincide in the class of complex projective algebraic manifolds.

## ["Meromorphic Convexity on Stein Manifolds"](https://arxiv.org/abs/2310.07066)
- Joint with [Rasul Shafikov](https://math.sci.uwo.ca/~shafikov/)
- To appear in: *Indiana University Mathematics Journal* (2025)
- Submitted
- Abstract:

> We consider generalizations of rational convexity to Stein manifolds and prove related results.

## ["Hypersurface Convexity and Extension of Kähler Forms"](https://doi.org/10.48550/arXiv.2310.02132)
- Joint with [Purvi Gupta](https://math.iisc.ac.in/~purvigupta/) and [Rasul Shafikov](https://math.sci.uwo.ca/~shafikov/)
- To appear in: *Mathematische Zeitschrift* (2025)
- Submitted
- Abstract:

> The following generalization of a result of S. Nemirovski is proved: If `$X$` is either a projective or a Stein manifold and `$K\subset X$` is a compact sublevel set of a strictly plurisubharmonic function `$\varphi$` defined in a neighborhood of `$K$`, then `$X\setminus K$` is a union of positive divisors if and only if `$dd^c\varphi$` extends to a Hodge form on `$X$`.

## ["Relationships Between the Bergman Kernels of Hartogs Domains and Their Base"](https://arxiv.org/abs/2208.14291)
- Preprint (Updated 2024)
- Submitted
- Abstract:

> We explore the relationship between the Bergman kernel of a Hartogs domain and weighted Bergman kernels over its base domain. In particular we develop a representation of the Bergman kernel of a Hartogs domain as a series involving weighted Bergman kernels over its base, as well as a "transformation" formula for some weighted Bergman kernels. Other relationships of this type are presented.

## ["On Rational Convexity of Totally Real Sets"](https://doi.org/10.1112/blms.12842)
- Joint with [Rasul Shafikov](https://math.sci.uwo.ca/~shafikov/)
- Published in: *Bulletin of the London Mathematical Society* **55**, 2158--2175 (2023).
- Abstract:

>Under a mild technical assumption, we prove a necessary and sufficient condition for a totally real compact set in `$\mathbb{C}^n$` to be rationally convex. This generalizes a classical result of Duval–Sibony.

## ["On the Dimension of the Bergman Space of Some Hartogs Domains with Higher Dimensional Bases"](https://doi.org/10.1007/s12220-020-00557-1)
- Published in: *The Journal of Geometric Analysis* **31**, 7885--7899 (2021).
- Abstract:

>Let `$D$` be a Hartogs domain of the form `$D=D_{\varphi}(G)=\{(z,w)\in G\times\mathbb{C}^N\,:\,\|w\|<e^{-\varphi(x)}\}$`, where `$\varphi$` is a plurisubharmonic function on `$G$` and
>`$G\subseteq\mathbb{C}^M$` is a pseudoconvex domain. We expand on the results of Jucha (J Geom Anal 22(1):23-37, 2012) and prove the infinite-dimensionality or trivial of the space of square
>integrable holomorphic functions on `$D_\varphi(G)$` for various choices of `$\varphi$` and `$G$`.

## ["Equivalent Bergman Spaces with Inequivalent Weights"](https://doi.org/10.1007/s12220-018-9986-5)
- Published in: *The Journal of Geometric Analysis* **29**, 217--223 (2019).
- Abstract:

>We give a proof that every space of weighted square-integrable holomorphic functions admits an equivalent weight whose Bergman kernel has zeroes. Here the weights are equivalent in the sense
>that they determine the same space of holomorphic functions. Additionally, a family of radial weights in `$L^1(\mathbb{C})$` whose associated Bergman kernels that have infinitely many zeroes
>is exhibited.
